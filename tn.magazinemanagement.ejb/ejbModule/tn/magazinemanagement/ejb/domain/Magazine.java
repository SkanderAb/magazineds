package tn.magazinemanagement.ejb.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Magazine
 *
 */
@Entity
@Table(name = "t_magazine")
public class Magazine implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private int idMagazine;
	private String name;
	private String format;
	private List <Article> articles;

	public Magazine() {
		super();
	}

	public Magazine(int idMagazine, String name, String format,
			List<Article> articles) {
		super();
		this.idMagazine = idMagazine;
		this.name = name;
		this.format = format;
		this.articles = articles;
	}

	public Magazine(String name, String format) {
		super();
		this.name = name;
		this.format = format;
	}

	@Id
	@GeneratedValue
	public int getIdMagazine() {
		return idMagazine;
	}

	public void setIdMagazine(int idMagazine) {
		this.idMagazine = idMagazine;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	@OneToMany(mappedBy="magazine")
	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	@Override
	public String toString() {
		return "Magazine [idMagazine=" + idMagazine + ", name=" + name
				+ ", format=" + format + "]";
	}
   
}
