package tn.magazinemanagement.ejb.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Entity implementation class for Entity: Section
 *
 */
@Entity
public class Section implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private int idSection;
	private String label;
	private List<Article> articles;

	public Section() {
		super();
	}

	public Section(int idSection, String label, List<Article> articles) {
		super();
		this.idSection = idSection;
		this.label = label;
		this.articles = articles;
	}

	@Id
	@GeneratedValue
	public int getIdSection() {
		return idSection;
	}

	public void setIdSection(int idSection) {
		this.idSection = idSection;
	}

	@Column(name = "t_label")
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@OneToMany(mappedBy="section",cascade = CascadeType.ALL)
	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	@Override
	public String toString() {
		return "Section [idSection=" + idSection + ", label=" + label + "]";
	}
   
}
