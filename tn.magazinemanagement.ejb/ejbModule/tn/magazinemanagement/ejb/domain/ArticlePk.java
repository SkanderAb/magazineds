package tn.magazinemanagement.ejb.domain;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: ArticlePk
 *
 */
@Embeddable
public class ArticlePk implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private int idMagazine;
	private String cinRedactor;
	private Calendar publicationDate;

	public ArticlePk() {
		super();
	}

	public ArticlePk(int idMagazine, String cinRedactor,
			Calendar publicationDate) {
		super();
		this.idMagazine = idMagazine;
		this.cinRedactor = cinRedactor;
		this.publicationDate = publicationDate;
	}

	@Column(name="idMagazinePk")
	public int getIdMagazine() {
		return idMagazine;
	}

	public void setIdMagazine(int idMagazine) {
		this.idMagazine = idMagazine;
	}

	@Column(name="cinRedactorPk")
	public String getCinRedactor() {
		return cinRedactor;
	}

	public void setCinRedactor(String cinRedactor) {
		this.cinRedactor = cinRedactor;
	}

	public Calendar getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Calendar publicationDate) {
		this.publicationDate = publicationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cinRedactor == null) ? 0 : cinRedactor.hashCode());
		result = prime * result + idMagazine;
		result = prime * result
				+ ((publicationDate == null) ? 0 : publicationDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArticlePk other = (ArticlePk) obj;
		if (cinRedactor == null) {
			if (other.cinRedactor != null)
				return false;
		} else if (!cinRedactor.equals(other.cinRedactor))
			return false;
		if (idMagazine != other.idMagazine)
			return false;
		if (publicationDate == null) {
			if (other.publicationDate != null)
				return false;
		} else if (!publicationDate.equals(other.publicationDate))
			return false;
		return true;
	} 
}
