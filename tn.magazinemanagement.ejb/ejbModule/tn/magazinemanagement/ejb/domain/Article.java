package tn.magazinemanagement.ejb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Article
 *
 */
@Entity
@Table(name = "t_article")
public class Article implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private ArticlePk articlePk;
	private String titre;
	private Section section;
	private Redactor redactor;
	private Magazine magazine;

	public Article() {
		super();
	}

	@EmbeddedId
	public ArticlePk getArticlePk() {
		return articlePk;
	}

	public void setArticlePk(ArticlePk articlePk) {
		this.articlePk = articlePk;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_section")
	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="cinRedactorPk",referencedColumnName="cinRedactor",insertable=false,updatable=false)
	public Redactor getRedactor() {
		return redactor;
	}

	public void setRedactor(Redactor redactor) {
		this.redactor = redactor;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="idMagazinePk",referencedColumnName="idMagazine",insertable=false,updatable=false)
	public Magazine getMagazine() {
		return magazine;
	}

	public void setMagazine(Magazine magazine) {
		this.magazine = magazine;
	}

	@Override
	public String toString() {
		return "Article [titre="
				+ titre + ", section=" + section + ", redactor=" + redactor
				+ ", magazine=" + magazine + "]";
	}
   
}
