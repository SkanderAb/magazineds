package tn.magazinemanagement.ejb.services.imp;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.magazinemanagement.ejb.domain.Article;
import tn.magazinemanagement.ejb.domain.ArticlePk;
import tn.magazinemanagement.ejb.domain.Magazine;
import tn.magazinemanagement.ejb.domain.Redactor;
import tn.magazinemanagement.ejb.domain.Section;
import tn.magazinemanagement.ejb.services.interfaces.MagazineServicesLocal;
import tn.magazinemanagement.ejb.services.interfaces.MagazineServicesRemote;

/**
 * Session Bean implementation class MagazineServices
 */
@Stateless
public class MagazineServices implements MagazineServicesRemote,
		MagazineServicesLocal {

	@PersistenceContext(name = "tn.magazinemanagement.ejb")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public MagazineServices() {
	}

	@Override
	public boolean createMagazine(Magazine magazine) {
		em.persist(magazine);
		return true;

	}

	@Override
	public boolean createRedactor(Redactor redactor) {
		em.persist(redactor);
		return true;
	}

	@Override
	public boolean createArticle(String nameMagazine, String nameRedactor,
			String lastNameRedactor, String titleArticle,
			Calendar publicationDate) {

		Article article = new Article();
		Magazine magazine = findMagazineByName(nameMagazine);
		Redactor redactor = findRedactorByComplexName(nameRedactor,
				lastNameRedactor);

		ArticlePk articlePk = new ArticlePk(magazine.getIdMagazine(),
				redactor.getCinRedactor(), publicationDate);
		article.setArticlePk(articlePk);
		article.setMagazine(magazine);
		article.setTitre(titleArticle);
		article.setRedactor(redactor);

		em.persist(article);
		return true;
	}

	@Override
	public boolean updateArticle(String title, String label) {

		Section section = new Section();
		section.setLabel(label);

		Article article = findArticleByTitle(title);
		article.setSection(section);

		em.persist(article);
		return true;
	}

	@Override
	public List<Article> findAllArticlesByParams(String cinRedactor,
			String nameRedactor, String lastNameRedactor, String sectionLabel) {

		TypedQuery<Article> query = em
				.createQuery(
						"SELECT A FROM Article A WHERE (A.redactor.cinRedactor = :cinRedactor Or A.redactor.firstName = :nameRedactor) Or (A.redactor.lastName = :lastNameRedactor Or A.section.label = :sectionLabel)",
						Article.class);
		query.setParameter("cinRedactor", cinRedactor);
		query.setParameter("nameRedactor", nameRedactor);
		query.setParameter("lastNameRedactor", lastNameRedactor);
		query.setParameter("sectionLabel", sectionLabel);

		List<Article> ArticleList = query.getResultList();
		return ArticleList;
	}

	@Override
	public Magazine findMagazineByName(String nameMagazine) {

		TypedQuery<Magazine> query = em.createQuery(
				"SELECT M FROM Magazine M WHERE M.name =:name", Magazine.class);
		query.setParameter("name", nameMagazine);
		return query.getSingleResult();
	}

	@Override
	public Redactor findRedactorByComplexName(String nameRedactor,
			String lastNameRedactor) {

		TypedQuery<Redactor> query = em
				.createQuery(
						"SELECT R FROM Redactor R WHERE R.firstName =:firstName AND R.lastName =:lastName",
						Redactor.class);
		query.setParameter("firstName", nameRedactor);
		query.setParameter("lastName", lastNameRedactor);
		return query.getSingleResult();
	}

	@Override
	public Article findArticleByTitle(String title) {

		TypedQuery<Article> query = em.createQuery(
				"SELECT A FROM Article A WHERE A.titre =:titre", Article.class);
		query.setParameter("titre", title);
		return query.getSingleResult();
	}

}
