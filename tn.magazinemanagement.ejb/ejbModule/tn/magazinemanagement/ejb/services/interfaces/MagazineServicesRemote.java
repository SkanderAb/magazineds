package tn.magazinemanagement.ejb.services.interfaces;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Remote;

import tn.magazinemanagement.ejb.domain.Article;
import tn.magazinemanagement.ejb.domain.Magazine;
import tn.magazinemanagement.ejb.domain.Redactor;

@Remote
public interface MagazineServicesRemote {

	boolean createMagazine(Magazine magazine);

	boolean createRedactor(Redactor redactor);

	boolean createArticle(String nameMagazine, String nameRedactor,
			String lastNameRedactor, String titleArticle,
			Calendar publicationDate);

	boolean updateArticle(String title, String label);

	List<Article> findAllArticlesByParams(String cinRedactor,
			String nameRedactor, String lastNameRedactor, String sectionLabel);

	Magazine findMagazineByName(String name);

	Redactor findRedactorByComplexName(String nameRedactor,
			String lastNameRedactor);

	Article findArticleByTitle(String title);

}
