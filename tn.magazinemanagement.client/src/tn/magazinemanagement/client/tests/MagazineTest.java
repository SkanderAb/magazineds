package tn.magazinemanagement.client.tests;

import static org.junit.Assert.assertTrue;
import static tn.magazinemanagement.client.delegate.MagazineManagementDelegate.createArticle;
import static tn.magazinemanagement.client.delegate.MagazineManagementDelegate.createMagazine;
import static tn.magazinemanagement.client.delegate.MagazineManagementDelegate.createRedactor;
import static tn.magazinemanagement.client.delegate.MagazineManagementDelegate.findAllArticlesByParams;
import static tn.magazinemanagement.client.delegate.MagazineManagementDelegate.updateArticle;

import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import tn.magazinemanagement.ejb.domain.Article;
import tn.magazinemanagement.ejb.domain.Magazine;

public class MagazineTest {

	/**
	 * Asserts the good adding of a predefined list of Magazines
	 */
	@Test
	public void testAddMagazine() {
		Magazine magazine = new Magazine("Assabah", "papier");
		assertTrue(createMagazine(magazine));

		Magazine magazine1 = new Magazine("JetSet Magazine", "electronique");
		assertTrue(createMagazine(magazine1));

		Magazine magazine2 = new Magazine("Le Figaro", "papier");
		assertTrue(createMagazine(magazine2));

		Magazine magazine3 = new Magazine("Java Magazine", "electronique");
		assertTrue(createMagazine(magazine3));

	}

	/**
	 * Asserts the good adding of a predefined list of Redactors
	 */
	@Test
	public void testAddRedactor() {
		assertTrue(createRedactor("09999999", "Kamel", "Ben Youssef"));
		assertTrue(createRedactor("07777777", "Ramzy", "Malouki"));
		assertTrue(createRedactor("06666666", "Benjamin", "Ferran"));
		assertTrue(createRedactor("05555555", "Tom", "Haunert"));

	}

	/**
	 * Asserts the good adding of a predefined list of Articles Assigns a
	 * redactor and a magazine to an article using the following data :
	 * nameMagazine nameRedactor lastNameRedactor titleArticle
	 * 
	 */
	@Test
	public void testAddArticle() {

		String nameMagazine = "Assabah";
		String nameRedactor = "Kamel";
		String lastNameRedactor = "Ben Youssef";
		String titleArticle = "La constitution tunisienne au crible";
		assertTrue(createArticle(nameMagazine, nameRedactor, lastNameRedactor,
				titleArticle, Calendar.getInstance()));

		 nameMagazine = "JetSet Magazine";
		 nameRedactor = "Ramzy";
		 lastNameRedactor = "Malouki";
		 titleArticle = "Interview de Matt Damon";
		 assertTrue(createArticle(nameMagazine, nameRedactor,
		 lastNameRedactor,
		 titleArticle, Calendar.getInstance()));

		 nameMagazine = "Java Magazine";
		 nameRedactor = "Tom";
		 lastNameRedactor = "Haunert";
		 titleArticle = "Big Data Processing with Java";
		 assertTrue(createArticle(nameMagazine, nameRedactor,
		 lastNameRedactor,
		 titleArticle, Calendar.getInstance()));

		nameMagazine = "Le Figaro";
		nameRedactor = "Benjamin";
		lastNameRedactor = "Ferran";
		titleArticle = "Gmail f�te ses 10 ans";
		assertTrue(createArticle(nameMagazine, nameRedactor, lastNameRedactor,
				titleArticle, Calendar.getInstance()));
	}

	/**
	 * Asserts the good assignment of a section to an article
	 */
	@Test
	public void testAssignSectionToArticle() {
		assertTrue(updateArticle("Gmail f�te ses 10 ans", "Web Tech."));
		assertTrue(updateArticle("Big Data Processing with Java", "Programming"));
	}

	/**
	 * Asserts good fetching of articles while : Article is published by a
	 * giving Magazine Article is edited by a giving Redactor Article belongs to
	 * a giving section
	 */
	@Test
	public void testFindAllArticleByParams() {
		List<Article> articles = findAllArticlesByParams("Le Figaro",
				"Benjamin", "Ferran", "Web Tech.");

		System.out.println("===> " + articles.size());

		Assert.assertEquals(articles.size(), 1);

		System.out.println(articles);
	}

}
