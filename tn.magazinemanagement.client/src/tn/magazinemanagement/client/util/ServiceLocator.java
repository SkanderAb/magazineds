package tn.magazinemanagement.client.util;

import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ServiceLocator {
	private Context context;
	private Map<String, Object> cache;
	private static ServiceLocator instance;

	private ServiceLocator() {
		cache = new HashMap<String, Object>();
		try {
			context = new InitialContext();
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	public static ServiceLocator getInstance() {
		if (instance == null)
			instance = new ServiceLocator();
		return instance;
	}

	public synchronized Object getProxy(String jndi) {
		Object object = null;
		if (cache.get(jndi) != null)
			return cache.get(jndi);
		else {
			try {
				object = context.lookup(jndi);
				if (object != null) {
					cache.put(jndi, object);
				}
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		return object;
	}

}
