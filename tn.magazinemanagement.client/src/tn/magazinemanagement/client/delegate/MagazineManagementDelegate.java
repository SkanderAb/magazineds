package tn.magazinemanagement.client.delegate;

import java.util.Calendar;
import java.util.List;

import tn.magazinemanagement.client.util.ServiceLocator;
import tn.magazinemanagement.ejb.domain.Article;
import tn.magazinemanagement.ejb.domain.Magazine;
import tn.magazinemanagement.ejb.domain.Redactor;
import tn.magazinemanagement.ejb.services.interfaces.MagazineServicesRemote;

public class MagazineManagementDelegate {
	static final String jndiName = "ejb:/tn.magazinemanagement.ejb/MagazineServices!tn.magazinemanagement.ejb.services.interfaces.MagazineServicesRemote";

	private static MagazineServicesRemote getProxy() {
		return (MagazineServicesRemote) ServiceLocator.getInstance().getProxy(
				jndiName);
	}

	public static boolean createMagazine(Magazine magazine) {
		return getProxy().createMagazine(magazine);
	}

	public static boolean createRedactor(String cinRedactor, String firstName,
			String lastName) {
		return getProxy().createRedactor(
				new Redactor(cinRedactor, firstName, lastName));
	}

	public static boolean createArticle(String nameMagazine,
			String nameRedactor, String lastNameRedactor, String titleArticle,
			Calendar publicationDate) {
		return getProxy().createArticle(nameMagazine, nameRedactor,
				lastNameRedactor, titleArticle, publicationDate);
	}

	public static boolean updateArticle(String title, String label) {
		return getProxy().updateArticle(title, label);
	}

	public static List<Article> findAllArticlesByParams(String cinRedactor,
			String nameRedactor, String lastNameRedactor, String sectionLabel) {
		return getProxy().findAllArticlesByParams(cinRedactor, nameRedactor,
				lastNameRedactor, sectionLabel);
	}

}
